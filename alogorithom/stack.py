class Stack:
	def __init__(self):
 		self.items = []
 	def push(self,item):
 		return self.items.append(item)

 	def pop(self,item):
 		return self.items.pop()

 	def get_stack(self):
 		return self.items


 s = Stack()
 s.push("a")
 s.push("b")
 print(s.get_stack())
 s.pop()
 print(s.get_stack())