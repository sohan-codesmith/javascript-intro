//challenge 1 : your age in Days

const ageInDays = (()=>{
	let birthYear = prompt('what year were you born ...Good Friend ?');
	let ageInDayss = (2020- birthYear) * 365;
	console.log(ageInDayss);
	let h1 = document.createElement('h1');
	h1.setAttribute('id','ageInDays');
	let textAnswer = document.createTextNode('Today you are '+ ageInDayss + ' days ');
	h1.appendChild(textAnswer);
	document.getElementById('flex-box-result').appendChild(h1);
});

const reset = (()=>{
	document.getElementById('ageInDays').remove();

});

const generateCat = (()=>{
	let image = document.createElement('img');
	let div = document.getElementById('flex-cat-gen');
	image.src = "http://thecatapi.com/api/images/get?format=src&type=gif&size=small";
	div.appendChild(image);
});

const rpsGame = ((yourChoice)=>{
	console.log(yourChoice.src);
	
	let humanChoice,botChoice;
	humanChoice = yourChoice.id;
	console.log(`your choices: ${humanChoice}`);
	
	botChoice = numToChoice(randToRpsInt());
	console.log(`computer choices: ${botChoice}`);
	
	results = decideWinder(humanChoice,botChoice);
	console.log(results);
	
	message = finalMessage(results);
	console.log(message);
	rpsFrontEnd(yourChoice.id,botChoice,message);
});


const randToRpsInt = (()=>{
	return Math.floor(Math.random()*3)
});

console.log(randToRpsInt());

const numToChoice = ((number)=>{
	return ['rock','paper','scissors'][number];
});

const decideWinder = ((humanChoice,botChoice)=>{

	let rpsDataBase = {
		'rock': {'scissors':1,'rock':0.5,'paper':0},
		'paper':{'rock':1,'paper':0.5,'scissors':0},
		'scissors':{'rock':0,'paper':1,'scissors':0.5}
	}
	let yourChoice = rpsDataBase[humanChoice][botChoice];
	let computerChoice = rpsDataBase[botChoice][humanChoice];

	return [yourChoice,computerChoice];

});

function finalMessage([yourChoice,computerChoice]){
	if(yourChoice === 0){
		return {'message':'You Lost !','color':'red'};
	}
	else if (yourChoice ===0.5){
		return {'message':'You Tied !','color':'yellow'};
	}
	else{
		return {'message':'You Won !','color':'green'};	
	}

} 

function rpsFrontEnd(humanImageChoices,botImageChoices,finalMessage){
	let imageDatabase = {
		'rock':document.getElementById('rock').src,
		'paper':document.getElementById('paper').src,
		'scissors':document.getElementById('scissors').src
	}
	//lets remove all the images
	document.getElementById('rock').remove();
	document.getElementById('paper').remove();
	document.getElementById('scissors').remove();

	let humanDiv = document.createElement('div');
	let botDiv = document.createElement('div');
	let messageDiv = document.createElement('div');
	messageDiv.setAttribute('id','messageDivId');
	humanDiv.setAttribute('id','humanDivId');
	botDiv.setAttribute('id','botDivId');

	humanDiv.innerHTML = "<img src='" + imageDatabase[humanImageChoices] + "' height=150 width=150 style='box-shadow:0px 10px 50px rgba(37,50,233,1);'>'"
	botDiv.innerHTML = "<img src='" + imageDatabase[botImageChoices] + "' height=150 width=150 style='box-shadow:0px 10px 50px rgba(243,38,24,1);'>'" 
	messageDiv.innerHTML = "<h1 style='color:" + finalMessage['color'] + "; font-size:60px;padding:30px;'>"+finalMessage['message'] +""
	document.getElementById('flex-box-rps-div').appendChild(humanDiv);
	document.getElementById('flex-box-rps-div').appendChild(messageDiv);
	document.getElementById('flex-box-rps-div').appendChild(botDiv);
}

const retry = (()=>{
	console.log("Retrying..");
	// document.getElementById('messageDivId').remove();
	// document.getElementById('humanDivId').remove();
	// document.getElementById('botDivId').remove();


	document.getElementById('humanDivId').innerHTML = "<img id='rock' src='static/images/rock.jpg' height=150 width=150 onclick='rpsGame(this)'>"
	document.getElementById('botDivId').innerHTML = "<img id='scissors' src='static/images/scissors.jpg' height=150 width=150 onclick='rpsGame(this)'>"
	document.getElementById('messageDivId').innerHTML = "<img id='paper' src='static/images/paper.jpg' height=150 width=150 onclick='rpsGame(this)'>"

	// humanDivId.innerHTML = "<img src='" + imageDatabase[rock] + "' height=150 width=150 style='box-shadow:0px 10px 50px rgba(37,50,233,1);'>'"
});




