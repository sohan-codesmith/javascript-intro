//author sohan

console.log("welcome to javascript");
// let age = prompt("what is your age? ");
document.getElementById('demo').innerHTML = "Hello";

let fruit = 'banana,apple,orange,grapes';

console.log(fruit.length);
console.log(fruit.indexOf('ana'));
console.log(fruit.slice(0,6));
console.log(fruit.replace('ban','123'));
console.log(fruit.toUpperCase(fruit)); 
console.log(fruit.charAt(2));
const newFruit = fruit.split(',');
console.log(newFruit);
console.log(newFruit.join('-'));
//print array element by two ways

//using loops

for(let i=1;i<newFruit.length;i++){
	console.log(newFruit[i]);
}
//using forEach 
newFruit.forEach((item)=>{
	console.log(item);
});

//use of push and pop in an array

//remove last elements from a array
newFruit.pop();
console.log(newFruit);

//append a element at the end of the array
newFruit.push('mango');
console.log(newFruit);

//append new element without using push
newFruit[newFruit.length] = 'lichi';
console.log(newFruit);

//remove first element
newFruit.shift();
console.log(newFruit);

//add first element to an array 
newFruit.unshift('kiwi');
console.log(newFruit);

//add two array
const allFruits = fruit.concat(newFruit);
console.log(allFruits);

let someNumbers = [1,34,235,2,45,346,56,7,86,23,543,232]

//sorted array ascending and decending order
console.log(someNumbers.sort(function(a,b){return a-b}));
//es6
console.log(someNumbers.sort((a,b)=>{return a-b}));
console.log(someNumbers.sort((a,b)=>{return b-a}));

//empthy array and push some element
let empthyArray = new Array();
for (let num=0;num<=10;num++){
	empthyArray.push(num);
}
console.log(empthyArray);

//object oriented way
let student ={
	first : "sohanur",
	last : "rahman",
	fullName: function(){ 
		return `your full name is : ${this.first} ${this.last}`;
	}
}
console.log(student.first); 
console.log(student['first']);
console.log(student.fullName());

//json manipulation 

let students = `[
{
	"name":"sohan",
	"age":25,
	"roll":1
},
{
	"name":"kabir",
	"age":28,
	"roll":3
}
]` 

students = JSON.parse(students);
console.log(students[0].name);

document.getElementById("json").innerHTML = students[1].name;